# taktics-simple-test

## Prerequisites
The solution must be retrieved with the technology you are used to.

The solution must include execution instructions.

If the reviewer can't execute the solution, it will count as failed.

## Test requirements
We want a webpage with basic session handling:

- An user must be able to login with username and password
- If the user doesn't exists, should show an error to the user
- If the login is correct, the page should navigate to a recipes page

We want to handle a repository of recipes. A recipe will have the following information:
  
- A type, can be: main course, starter or dessert
- A name
- A date of creation
- An image
- A difficulty rate
- An explanation about the recipe

Once logged, the user will see a list of recipes with the following columns:

- The image as a preview small image
- The recipe type
- The name
- The date of creation

This list can be filtered by:

- name
- date
- recipe type
- difficulty rate

The user then can create a new recipe, edit one already created or delete one or multiple recipes.

To create a new recipe, the app will open a new page in which the user has to provide the recipe data mentioned above.
All fields are required but:

- The image
- The difficulty rate

The user also can edit an existing recipe, the app will open a form including the data of the editing recipe.

Finally the user can delete the recipe.

BONUS 1: The user can export one or multiple recipes to an spreadsheet the list of recipes. The columns of the spreadsheet will be the same of the list and the filters.

BONUS 2: The user can send by email one recipe to an email.

## Delivering instructions
Create a repo of your choice with the solution and invite jplaza@taktics.net to the repository and the minimal permissions to download or clone the repository.

IMPORTANT: Please, do not fork this project.